"""
Welcome to PyDune!

To start playing with the Dune blockchain you need to get a PyDuneClient instance.
Just type:

>>> from pydune import pydune
>>> pydune

And follow the interactive documentation.
"""

from pydune.rpc import RpcProvider, localhost, mainnet, testnet, devnet
from pydune.crypto import Key
from pydune.proto import Proto
from pydune.michelson.contract import Contract
from pydune.client import PyDuneClient
from pydune.operation.group import OperationGroup
from pydune.michelson.interface import ContractInterface

pydune = PyDuneClient()
